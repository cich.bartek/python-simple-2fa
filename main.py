import tkinter as tk
from tkinter import messagebox
import pyotp
import qrcode
import os
import mysql.connector
import argon2

ph = argon2.PasswordHasher(time_cost = 32, memory_cost = 2**15, parallelism = 4, hash_len = 64, salt_len = 32, encoding = "utf-8", type = argon2.Type.ID)

# Aktualizacja kolekcji użytkowników z wyznaczeniem skrótów na bazie jawnych haseł.
#print(ph.hash('12345'))

# Create connection with database (using library Psycopg2)
connect = mysql.connector.connect(host="localhost", user="root", port="3306", database="baza_uwierzytelnienie")
cur = connect.cursor()

# key for authenticator
key = 'UHRNOGRCWNL7P3CRKLBFSXQOCJ4CXW65'
uri = pyotp.totp.TOTP(key).now()
# print(uri)
#create QR 
qrcode.make(uri).save("totp.png")

# def hashPassword(password):
#     return ph.hash(password)
# cur.execute(f'UPDATE users SET password = "{hashPassword("Haslo123")}" WHERE username = "Karolina" ')
# connect.commit()


def login():
    # Get the values of the login form fields
    username = username_entry.get()
    password = password_entry.get()
    verification_code = verification_code_entry.get()

    # Perform login logic here
    cur.execute(f"SELECT username, password FROM users WHERE username = '{username}' ")
    user = cur.fetchone()
    try:
        if user and user[0] == username and ph.verify(str(user[1]), str(password)) and verification_code == uri:
            messagebox.showinfo('Login', 'Zostałeś zalogowany poprawnie')
        else:
            messagebox.showerror('Login', 'Podano nieporawny login, hasło lub kod weryfikacyjny')
    except argon2.exceptions.VerifyMismatchError as e:
        messagebox.showerror("Błędne hasło!", e)
    except argon2.exceptions.InvalidHash as e:
        messagebox.showerror("Błędna wartość skrótu!", e)

# Create the main window
root = tk.Tk()
root.title("Logowanie")
root.geometry('250x120')

# Create the label for the username field
username_label = tk.Label(root, text="Login: ")
username_label.grid(row=0, column=0)

# Create the entry field for the username
username_entry = tk.Entry(root)
username_entry.grid(row=0, column=1)

# Create the label for the password field
password_label = tk.Label(root, text="Hasło: ")
password_label.grid(row=1, column=0)

# Create the entry field for the password
password_entry = tk.Entry(root, show="*")
password_entry.grid(row=1, column=1)

# Create the label for the verification code field
verification_code_label = tk.Label(root, text="Kod weryfikacyjny: ")
verification_code_label.grid(row=2, column=0)

# Create the entry field for the verification code
verification_code_entry = tk.Entry(root)
verification_code_entry.grid(row=2, column=1)

# Create the login button
login_button = tk.Button(root, text="Zaloguj się", command=login)
login_button.grid(row=3, column=1)

#Removing the image with the QR code, after clicking the button (X)
def delete_png():
    os.remove("totp.png")
    root.destroy()

root.protocol("WM_DELETE_WINDOW", delete_png)
# Run the main loop
root.mainloop()