# Python Simple 2FA

### Python - Simple project Two Factor Autentication with Tkinter.

##### App look
![first](https://gitlab.com/cich.bartek/python-simple-2fa/-/raw/main/pics/first.png)

##### Entering data
![second](https://gitlab.com/cich.bartek/python-simple-2fa/-/raw/main/pics/second.png)

##### Using a QR code scanner to read the verification code
![third](https://gitlab.com/cich.bartek/python-simple-2fa/-/raw/main/pics/third.png)

##### Once all the data has been entered correctly, a message is displayed that you have logged in correctly
![fourth](https://gitlab.com/cich.bartek/python-simple-2fa/-/raw/main/pics/fourth.png)

##### If incorrect data is given, an error message will be displayed
![fifth](https://gitlab.com/cich.bartek/python-simple-2fa/-/raw/main/pics/fifth.png)
